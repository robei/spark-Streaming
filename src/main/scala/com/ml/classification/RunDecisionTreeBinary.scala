package com.ml.classification

import com.ml.Chart
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.storage.StorageLevel
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.evaluation._
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.mllib.tree.DecisionTree
import org.apache.spark.mllib.tree.model.DecisionTreeModel
import org.joda.time._
import org.jfree.data.category.DefaultCategoryDataset

/**
  * Created by zghgchao 2017/12/13 12:43
  *
  * 决策树二元分类
  */
object RunDecisionTreeBinary {

  def main(args: Array[String]): Unit = {
    SetLogger()
    val sc = new SparkContext(new SparkConf().setAppName("App").setMaster("local[4]"))
    println("RunDecisionTreeBinary")
    println("==========数据准备阶段===============")
    val (trainData, validationData, testData, categoriesMap) = PrepareData(sc)
    trainData.persist()
    validationData.persist()
    testData.persist()
    println("==========训练评估阶段===============")
    println()
    print("是否需要进行参数调校 (Y:是  N:否) ? ")
    if (readLine() == "Y") {
      val model = parametersTunning(trainData, validationData)
      println("==========测试阶段===============")
      val auc = evaluateModel(model, testData)
      println("使用testata测试最佳模型,结果 AUC:" + auc)
      println("==========预测数据===============")
      PredictData(sc, model, categoriesMap)
    } else {
      val model = trainEvaluate(trainData, validationData)
      println("==========测试阶段===============")
      val auc = evaluateModel(model, testData)
      println("使用testata测试最佳模型,结果 AUC:" + auc)
      println("==========预测数据===============")
      PredictData(sc, model, categoriesMap)
    }

    trainData.unpersist();
    validationData.unpersist();
    testData.unpersist()
  }

  /**
    * 数据准备阶段
    * @param sc
    * @return
    */
  def PrepareData(sc: SparkContext): (RDD[LabeledPoint], RDD[LabeledPoint], RDD[LabeledPoint], Map[String, Int]) = {
    //----------------------1.导入并转换数据-------------
    print("开始导入数据...")
    val rawDataWithHeader = sc.textFile("file:///C:\\soft\\opt\\java\\sparktrain\\data\\train.tsv")
    //读取的是带表头的RDD
    val rawData = rawDataWithHeader.mapPartitionsWithIndex { (idx, iter) => if (idx == 0) iter.drop(1) else iter }
    //删除第一行数据（表头）RDD
    val lines = rawData.map(_.split("\t")) //读取每行数据
    println("共计：" + lines.count.toString() + "条")
    //----------------------2.创建训练评估所需数据 RDD[LabeledPoint]-------------
    //创建网页分类对照表
    val categoriesMap = lines.map(fields => fields(3)).distinct.collect.zipWithIndex.toMap
    //distinct去重，collect转换成Array，zipWithIndex转换成数字，toMap转换成对照表
    //labelPointRDD由label和Feature组成，Feature由categoryFeature（分类特征字段）和numericalFeature（数值特征字段）组成
    val labelpointRDD = lines.map {
      fields =>// 处理每一行数据
      val trFields = fields.map(_.replaceAll("\"", ""))
      val categoryFeaturesArray = Array.ofDim[Double](categoriesMap.size)
      //分类特征字段
      val categoryIdx = categoriesMap(fields(3))
      categoryFeaturesArray(categoryIdx) = 1
      val numericalFeatures = trFields.slice(4, fields.size - 1) //字段4-25，数值特征字段
        .map(d => if (d == "?") 0.0 else d.toDouble)
      val label = trFields(fields.size - 1).toInt //字段26
      LabeledPoint(label, Vectors.dense(categoryFeaturesArray ++ numericalFeatures))
    }
    //----------------------3.以随机方式将数据分为3个部分并且返回-------------

    val Array(trainData, validationData, testData) = labelpointRDD.randomSplit(Array(8, 1, 1))

    println("将数据分trainData:" + trainData.count() + "   validationData:" + validationData.count()
      + "   testData:" + testData.count())

    return (trainData, validationData, testData, categoriesMap) //返回数据
  }

  /**
    * 预测阶段，使用训练好的模型model,对test.tsv数据进行预测
    * @param sc
    * @param model
    * @param categoriesMap
    */
  def PredictData(sc: SparkContext, model: DecisionTreeModel, categoriesMap: Map[String, Int]): Unit = {
    //----------------------1.导入并转换数据-------------
    val rawDataWithHeader = sc.textFile("file:///C:\\soft\\opt\\java\\sparktrain\\data\\test.tsv")
    val rawData = rawDataWithHeader.mapPartitionsWithIndex { (idx, iter) => if (idx == 0) iter.drop(1) else iter }
    val lines = rawData.map(_.split("\t"))
    println("共计：" + lines.count.toString() + "条")
    //----------------------2.创建训练评估所需数据 RDD[LabeledPoint]-------------
    val dataRDD = lines.take(20).map { fields =>   //只读取20条进行预测
      val trFields = fields.map(_.replaceAll("\"", ""))
      val categoryFeaturesArray = Array.ofDim[Double](categoriesMap.size)
      val categoryIdx = categoriesMap(fields(3))
      categoryFeaturesArray(categoryIdx) = 1
      val numericalFeatures = trFields.slice(4, fields.size)
        .map(d => if (d == "?") 0.0 else d.toDouble)
      val label = 0
      //----------------------3进行预测-------------
      val url = trFields(0)
      val Features = Vectors.dense(categoryFeaturesArray ++ numericalFeatures)
      val predict = model.predict(Features).toInt
      var predictDesc = {
        predict match {
          case 0 => "暂时性网页(ephemeral)";
          case 1 => "长青网页(evergreen)";
        }
      }
      println("网址：  " + url + "==>预测:" + predictDesc)
    }

  }

  /**
    * 训练评估
    *
    * @param trainData      训练集
    * @param validationData 验证集
    * @return
    */
  def trainEvaluate(trainData: RDD[LabeledPoint], validationData: RDD[LabeledPoint]): DecisionTreeModel = {
    print("开始训练...")
    //传入参数：trainData，impurity(Gini/entropy)，maxDepth，maxBins
    val (model, time) = trainModel(trainData, "entropy", 5, 5)
    println("训练完成,所需时间:" + time + "毫秒")
    val AUC = evaluateModel(model, validationData)
    println("评估结果AUC=" + AUC)
    return (model)
  }

  /**
    * 训练模型
    *
    * @param trainData 训练集
    * @param impurity  决策树的评估方法：①Gini ②entropy熵
    * @param maxDepth  决策树的最大深度
    * @param maxBins   决策树每一个节点的最大分支数目
    * @return 返回模型与训练时间
    */
  def trainModel(trainData: RDD[LabeledPoint], impurity: String, maxDepth: Int, maxBins: Int): (DecisionTreeModel, Double) = {
    val startTime = new DateTime()
    /**
      * trainClassifier(input: RDD[LabeledPoint], numClasses: Int, categoricalFeaturesInfo: Map[Int, Int],
      * impurity: String, maxDepth: Int, maxBins: Int): DecisionTreeModel
      * input：训练集，numClasses：分类数目，categoricalFeaturesInfo：Map[Int, Int]()，
      * impurity：决策树的评估方法：①Gini ②entropy熵
      * maxDepth
      * maxBins
      */
    val model = DecisionTree.trainClassifier(trainData, 2, Map[Int, Int](), impurity, maxDepth, maxBins)
    val endTime = new DateTime()
    val duration = new Duration(startTime, endTime) //计算训练所需时间
    (model, duration.getMillis())
  }

  /**
    * 以AUC（Area under the Curve of ROC）评估数据模型
    *
    * @param model
    * @param validationData
    * @return
    */
  def evaluateModel(model: DecisionTreeModel, validationData: RDD[LabeledPoint]): (Double) = {
    //scoreAndLabels由与score（预测结果）与labels（真实的结果）
    val scoreAndLabels = validationData.map { data =>
      var predict = model.predict(data.features)
      (predict, data.label)
    }
    val Metrics = new BinaryClassificationMetrics(scoreAndLabels)
    val AUC = Metrics.areaUnderROC
    //返回AUC
    (AUC)
  }

  /**
    * parametersTunning参数调校
    * @param trainData
    * @param validationData
    * @return
    */
  def parametersTunning(trainData: RDD[LabeledPoint], validationData: RDD[LabeledPoint]): DecisionTreeModel = {
    println("-----评估 Impurity参数使用 gini, entropy---------")
    evaluateParameter(trainData, validationData, "impurity", Array("gini", "entropy"), Array(10), Array(10))
    println("-----评估MaxDepth参数使用 (3, 5, 10, 15, 20)---------")
    evaluateParameter(trainData, validationData, "maxDepth", Array("gini"), Array(3, 5, 10, 15, 20, 25), Array(10))
    println("-----评估maxBins参数使用 (3, 5, 10, 50, 100)---------")
    evaluateParameter(trainData, validationData, "maxBins", Array("gini"), Array(10), Array(3, 5, 10, 50, 100, 200))
    println("-----所有参数交叉评估找出最好的参数组合---------")
    val bestModel = evaluateAllParameter(trainData, validationData, Array("gini", "entropy"),
      Array(3, 5, 10, 15, 20), Array(3, 5, 10, 50, 100))
    return (bestModel)
  }

  /**
    * evaluateParameter评估单个参数，看哪一个参数具有比较好的准确率，并画出图形
    * @param trainData
    * @param validationData
    * @param evaluateParameter
    * @param impurityArray
    * @param maxdepthArray
    */
  def evaluateParameter(trainData: RDD[LabeledPoint], validationData: RDD[LabeledPoint],
                        evaluateParameter: String, impurityArray: Array[String], maxdepthArray: Array[Int], maxBinsArray: Array[Int]) = {

    var dataBarChart = new DefaultCategoryDataset()
    var dataLineChart = new DefaultCategoryDataset()

    for (impurity <- impurityArray; maxDepth <- maxdepthArray; maxBins <- maxBinsArray) {
      val (model, time) = trainModel(trainData, impurity, maxDepth, maxBins)
      val auc = evaluateModel(model, validationData)

      val parameterData =
        evaluateParameter match {
          case "impurity" => impurity;
          case "maxDepth" => maxDepth;
          case "maxBins" => maxBins
        }
      dataBarChart.addValue(auc, evaluateParameter, parameterData.toString())
      dataLineChart.addValue(time, "Time", parameterData.toString())
    }
    Chart.plotBarLineChart("DecisionTree evaluations " + evaluateParameter, evaluateParameter, "AUC", 0.58, 0.7, "Time", dataBarChart, dataLineChart)
  }

  def evaluateAllParameter(trainData: RDD[LabeledPoint], validationData: RDD[LabeledPoint], impurityArray: Array[String], maxdepthArray: Array[Int], maxBinsArray: Array[Int]): DecisionTreeModel = {
    val evaluationsArray =
      for (impurity <- impurityArray; maxDepth <- maxdepthArray; maxBins <- maxBinsArray) yield {
        val (model, time) = trainModel(trainData, impurity, maxDepth, maxBins)
        val auc = evaluateModel(model, validationData)
        (impurity, maxDepth, maxBins, auc)
      }
    val BestEval = (evaluationsArray.sortBy(_._4).reverse) (0)
    println("调校后最佳参数：impurity:" + BestEval._1 + "  ,maxDepth:" + BestEval._2 + "  ,maxBins:" + BestEval._3
      + "  ,结果AUC = " + BestEval._4)
    val (bestModel, time) = trainModel(trainData.union(validationData), BestEval._1, BestEval._2, BestEval._3)
    return bestModel
  }

  def SetLogger() = {
    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("com").setLevel(Level.OFF)
    System.setProperty("spark.ui.showConsoleProgress", "false")
    Logger.getRootLogger().setLevel(Level.OFF);
  }
}
