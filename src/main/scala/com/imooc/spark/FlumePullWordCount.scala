package com.imooc.spark

import org.apache.spark.SparkConf
import org.apache.spark.streaming.flume.FlumeUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by zghgchao 2017/11/16 16:18
  * Spark Streaming 整合Flume的第一种方式
  */
object FlumePullWordCount {
  def main(args: Array[String]): Unit = {

    if (args.length != 2) {
      System.err.println("USage：FlumePushWordCount <hostname> <port>")
      System.exit(1)
    }

    val sparkConf = new SparkConf()//.setMaster("local[2]").setAppName("FlumePullWordCount")
    val ssc = new StreamingContext(sparkConf, Seconds(5))

    val Array(hostname, port) = args

    //TODO... Spark Streaming 整合Flume
    //    val flumeStream = FlumeUtils.createStream(ssc, "hadoop000", 41414)
    val flumeStream = FlumeUtils.createPollingStream(ssc, hostname, port.toInt) //本地测试

    flumeStream.map(x => new String(x.event.getBody.array()).trim)
      .flatMap(_.split(" ")).map((_, 1)).reduceByKey(_ + _).print()

    ssc.start()
    ssc.awaitTermination()
  }
}
