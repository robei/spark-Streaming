package com.imooc.spark

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by zghgchao 2017/11/15 10:41
  *  使用Spark Streaming处理文件系统（local/hdfs）的数据
  *     /c/tmp $ touch test.log
  *               vi test.log
  *                   a c d d d a a
  *               cp test.log  data/1.log
  *               cp test.log  data/2.log
  */
object FileWordCount {
  def main(args: Array[String]): Unit = {

    //local[n]，可以为1，因为不需要使用receiver接收数据
    val sparkConf = new SparkConf().setMaster("local").setAppName("FileWordCount")

    val ssc = new StreamingContext(sparkConf,Seconds(5))

    val lines = ssc.textFileStream("file:///C:/tmp/data")

    val result = lines.flatMap(_.split(" ")).map((_,1)).reduceByKey(_+_)

    result.print()

    ssc.start()
    ssc.awaitTermination()
  }
}
