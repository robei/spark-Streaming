package com.imooc.spark

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by zghgchao 2017/11/16 13:30
  * 黑名单过滤
  *         20180808,zs
  *         20180808,ls
  *        20180808,ww
  */
object TransformApp {
  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[2]").setAppName("TransformApp")

    /**
      * 创建StreamingContext需要两个参数：SparkConf和batch interval
      */
    val ssc = new StreamingContext(sparkConf, Seconds(5))

    /**
      * 构建黑名单
      */
    val blacks = List("zs", "ls")
    val blacksRDD = ssc.sparkContext.parallelize(blacks).map(x => (x, true))//(zs: true)  (ls: true)

    // input：20180808,zs
    val lines = ssc.socketTextStream("172.17.66.107", 9999)

    val clicklog = lines.map(x => (x.split(",")(1), x)).transform(rdd => {   //(zs,(20180808,zs))
      rdd.leftOuterJoin(blacksRDD)   //(zs: [<20180808,zs>, <true>])
        .filter(x => x._2._2.getOrElse(false) != true)
        .map(x => x._2._1)
    })

    clicklog.print()
    ssc.start()

    ssc.awaitTermination()

  }
}
