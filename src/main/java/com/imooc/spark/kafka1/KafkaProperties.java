package com.imooc.spark.kafka1;

/**
 * Created by zghgchao 2017/11/14 10:39
 * Kafka 常用配置文件
 */
public class KafkaProperties {

    public static final String ZK = "172.17.66.107:2181";//ZK地址及端口号

    public static final String TOPIC = "hello_topic";    //主题

    public static final String BROKER_LIST = "172.17.66.107:9092";//Kafka broker地址

    public static final String GROUP_ID = "test_group1";

}
