package com.imooc.spark.kafka1;

/**
 * Created by zghgchao 2017/11/14 11:24
 * Kafka Producer Java API 测试
 */
public class KafkaClientApp {
    public static void main(String[] args) {
        new KafkaProducer(KafkaProperties.TOPIC).start();
        new KafkaConsumer(KafkaProperties.TOPIC).start();
    }
}
